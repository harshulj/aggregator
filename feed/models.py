from django.db import IntegrityError, models
from django.utils import timezone
from time import mktime
from datetime import datetime
import feedparser
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)

class Category(models.Model):
    title           = models.CharField(max_length=63)

    def __unicode__(self):
        return "%s" % self.title

class Feed(models.Model):
    last_updated    = models.DateTimeField(blank=True)
    subtitle        = models.CharField(max_length=300, blank=True)
    link            = models.CharField(max_length=300)
    title           = models.CharField(max_length=300, blank=True)
    encoding        = models.CharField(max_length=15, blank=True)
    version         = models.CharField(max_length=15, blank=True)
    category        = models.ForeignKey(Category, related_name="feeds")

    def _get_feed(self):
        return feedparser.parse(self.link)

    def _get_feed_fields(self):
        logger.info("Starting fetching and parsing of feeds.")
        feed = self._get_feed()
        self.subtitle = feed['feed']['subtitle']
        if 'updated_parsed' in feed['feed']:
            self.last_updated = datetime.fromtimestamp(mktime(feed['feed']['updated_parsed']))
        else:
            self.last_updated = timezone.now()
        self.title = feed['feed']['title']
        self.encoding = feed['encoding']
        self.version = feed['version']

    def fill_entries(self):
        logger.info("Filling entries")
        feed = self._get_feed()
        for item in feed['entries']:
            item_id = None
            if 'id' in item:
                item_id = item['id']
            else:
                item_id = item['link']
            item = Entry.objects.create(title=item['title'], link=item['link'], summary=item['summary'], feed=self, entry_id=item_id, published_on = datetime.fromtimestamp(mktime(item['published_parsed'])))

    def save(self, *args, **kwargs):
        is_new = False
        if not self.pk:
            self._get_feed_fields()
            is_new = True
        logger.info("Saving the feed details.")
        super(Feed, self).save(*args, **kwargs)
        #if is_new == True:
        #    self.fill_entries()

    def __unicode__(self):
        return "%s" % self.link

class Entry(models.Model):
    title           = models.CharField(max_length=500)
    published_on    = models.DateTimeField()
    link            = models.CharField(max_length=500)
    summary         = models.CharField(max_length=3000)
    entry_id        = models.CharField(max_length=500, unique=True)
    feed            = models.ForeignKey(Feed, related_name="entries")

    @property
    def category(self):
        return "%s" % (self.feed.category)

    def save(self, *args, **kwargs):
        try:
            super(Entry, self).save( *args, **kwargs)
        except IntegrityError as e:
            logger.info("Entry already present. %s" % str(e))

    def __unicode__(self):
        return "%s" % (self.title)

