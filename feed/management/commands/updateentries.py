from django.core.management.base import BaseCommand, CommandError
from feed.models import Feed
import logging

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        feeds = Feed.objects.all()
        logger.info("Filling Entries")
        print "Filling entries"
        for feed in feeds:
            feed.fill_entries()
        logger.info( "Feed and entries updated.")
