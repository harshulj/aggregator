from django.contrib import admin
from .models import *

class FeedAdmin(admin.ModelAdmin):
    list_display = ('title', 'last_updated', 'category', 'link', 'version', 'encoding')

class CategoryAdmin(admin.ModelAdmin):
    pass

class EntryAdmin(admin.ModelAdmin):
    list_display = ('title', 'published_on', 'link', 'feed', 'category')

admin.site.register(Entry, EntryAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Feed, FeedAdmin)
