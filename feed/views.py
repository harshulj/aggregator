from django.shortcuts import render

def index(request, template='feed/index.html'):
    return render(request, template)
