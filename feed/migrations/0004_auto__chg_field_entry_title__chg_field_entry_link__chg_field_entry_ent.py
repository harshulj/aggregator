# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Entry.title'
        db.alter_column(u'feed_entry', 'title', self.gf('django.db.models.fields.CharField')(max_length=511))

        # Changing field 'Entry.link'
        db.alter_column(u'feed_entry', 'link', self.gf('django.db.models.fields.CharField')(max_length=511))

        # Changing field 'Entry.entry_id'
        db.alter_column(u'feed_entry', 'entry_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=511))

    def backwards(self, orm):

        # Changing field 'Entry.title'
        db.alter_column(u'feed_entry', 'title', self.gf('django.db.models.fields.CharField')(max_length=255))

        # Changing field 'Entry.link'
        db.alter_column(u'feed_entry', 'link', self.gf('django.db.models.fields.CharField')(max_length=255))

        # Changing field 'Entry.entry_id'
        db.alter_column(u'feed_entry', 'entry_id', self.gf('django.db.models.fields.CharField')(max_length=255, unique=True))

    models = {
        u'feed.category': {
            'Meta': {'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '63'})
        },
        u'feed.entry': {
            'Meta': {'object_name': 'Entry'},
            'entry_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '511'}),
            'feed': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'entries'", 'to': u"orm['feed.Feed']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '511'}),
            'published_on': ('django.db.models.fields.DateTimeField', [], {}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '4095'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '511'})
        },
        u'feed.feed': {
            'Meta': {'object_name': 'Feed'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'feeds'", 'to': u"orm['feed.Category']"}),
            'encoding': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_updated': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'subtitle': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'})
        }
    }

    complete_apps = ['feed']