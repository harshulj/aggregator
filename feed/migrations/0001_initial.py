# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Category'
        db.create_table(u'feed_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=63)),
        ))
        db.send_create_signal(u'feed', ['Category'])

        # Adding model 'Feed'
        db.create_table(u'feed_feed', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('last_updated', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('subtitle', self.gf('django.db.models.fields.CharField')(max_length=127, blank=True)),
            ('link', self.gf('django.db.models.fields.CharField')(max_length=127)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=127, blank=True)),
            ('encoding', self.gf('django.db.models.fields.CharField')(max_length=15, blank=True)),
            ('version', self.gf('django.db.models.fields.CharField')(max_length=15, blank=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(related_name='feeds', to=orm['feed.Category'])),
        ))
        db.send_create_signal(u'feed', ['Feed'])

        # Adding model 'Entry'
        db.create_table(u'feed_entry', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=127)),
            ('published_on', self.gf('django.db.models.fields.DateTimeField')()),
            ('link', self.gf('django.db.models.fields.CharField')(max_length=127)),
            ('summary', self.gf('django.db.models.fields.CharField')(max_length=1023)),
            ('entry_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=127)),
            ('feed', self.gf('django.db.models.fields.related.ForeignKey')(related_name='entries', to=orm['feed.Feed'])),
        ))
        db.send_create_signal(u'feed', ['Entry'])


    def backwards(self, orm):
        # Deleting model 'Category'
        db.delete_table(u'feed_category')

        # Deleting model 'Feed'
        db.delete_table(u'feed_feed')

        # Deleting model 'Entry'
        db.delete_table(u'feed_entry')


    models = {
        u'feed.category': {
            'Meta': {'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '63'})
        },
        u'feed.entry': {
            'Meta': {'object_name': 'Entry'},
            'entry_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '127'}),
            'feed': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'entries'", 'to': u"orm['feed.Feed']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '127'}),
            'published_on': ('django.db.models.fields.DateTimeField', [], {}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '1023'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '127'})
        },
        u'feed.feed': {
            'Meta': {'object_name': 'Feed'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'feeds'", 'to': u"orm['feed.Category']"}),
            'encoding': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_updated': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '127'}),
            'subtitle': ('django.db.models.fields.CharField', [], {'max_length': '127', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '127', 'blank': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'})
        }
    }

    complete_apps = ['feed']